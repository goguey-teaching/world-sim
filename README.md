# Start

```python worldsim.py```

# Play with the model

Open ```localhost:8080``` in a browser.

Change data in the menu, see what happens.