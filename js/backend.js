class BackEnd {
    constructor(settings, callback, tooltips = {}) {
        var _this = this;
        this.settings = settings;
        this.tooltips = tooltips;

        this.toogled = false;
        this.callback = callback;
        this.updates = [];
    }

    init() {
        let prevbe = document.getElementById('backend');
        if (prevbe) { prevbe.remove(); }
        let prevbeh = document.getElementById('backendhandle');
        if (prevbeh) { prevbeh.remove(); }
        
        this.div = document.createElement('div');
        this.div.innerHTML = '<h1 class="backendelem backendtitle">Settings</h1><div id="backendmain" class="backendmain"></div>';
        this.div.id = 'backend';
        this.div.className = 'backend backend_up';
        document.body.appendChild(this.div);

        this.main = document.getElementById('backendmain');

        this.handle = document.createElement('div');
        this.handle.innerHTML = '<div class="backendbar"></div>';
        this.handle.id = 'backendhandle';
        this.handle.className = 'backendhandle backendhandle_up';
        document.body.appendChild(this.handle);

        let _this = this;
        this.handle.addEventListener('click', function () { _this.toogle(); });

        let keys = Object.keys(this.settings);
        for (let k = 0; k < keys.length; k++) {
            this.create_widget(this.main, [keys[k]]);
        }
    }

    keychain_id(keychain) {
        let id = '';
        for (let k = 0; k < keychain.length; k++) {
            id += keychain[k];
        }
        return id;
    }

    get_value(keychain) {
        let dico = this.settings;
        for (let k = 0; k < keychain.length-1; k++) {
            dico = dico[keychain[k]];
        }
        return dico[keychain[keychain.length-1]];
    }

    get_tooltip(keychain) {
        let dico = this.tooltips;
        for (let k = 0; k < keychain.length-1; k++) {
            if (k == 0 && !dico[keychain[k]]) { return 'Who knows...'; }
            if (!dico[keychain[k]]) { return dico; }
            if (typeof(dico[keychain[k]]) == 'string') { return dico[keychain[k]]; }
            dico = dico[keychain[k]];
        }
        if (keychain.length == 1 && !dico[keychain[keychain.length-1]]) { return 'Who knows...'; }
        if (!dico[keychain[keychain.length-1]]) { return dico; }
        return dico[keychain[keychain.length-1]];
    }

    set_value(keychain, val) {
        let dico = this.settings;
        for (let k = 0; k < keychain.length-1; k++) {
            dico = dico[keychain[k]];
        }
        dico[keychain[keychain.length-1]] = val;
    }

    create_widget(parent, keychain, dico) {
        let div = document.createElement('div');
        div.id = `backend_${this.keychain_id(keychain)}`;
        div.className = 'backendelem backendline';
        parent.appendChild(div);

        this.add_label(div, keychain, dico);
        this.add_widget(div, keychain, dico);
    }

    add_label(parent, keychain) {
        let label = document.createElement('div');
        label.innerHTML = `${keychain[keychain.length-1]}:`;
        label.id = `backend_label_${this.keychain_id(keychain)}`;
        label.className = 'backendelem backendlabel';
        label.setAttribute('tooltip', this.get_tooltip(keychain));
        parent.appendChild(label);
    }

    add_widget(parent, keychain) {
        let _this = this;

        let addlistener = true;

        let widget = document.createElement('div');
        widget.id = `backend_widget_${this.keychain_id(keychain)}`;
        widget.className = 'backendelem backendwidget';
        widget.setAttribute('tooltip', this.get_tooltip(keychain));
        parent.appendChild(widget);

        switch(typeof(this.get_value(keychain))) {
            case 'number':
                widget.innerHTML = `<input class="backendinput" type="number" id="input_${this.keychain_id(keychain)}" min="0" max="10000" step="0.1" value="${this.get_value(keychain)}" tooltip="${this.get_tooltip(keychain)}"></input>`;
                break;
            case 'string':
                widget.innerHTML = `<input class="backendinput" type="text" id="input_${this.keychain_id(keychain)}" value="${this.get_value(keychain)}" tooltip="${this.get_tooltip(keychain)}"></input>`;
                break;
            case 'object':
                var obj = this.get_value(keychain);
                if (obj.val !== undefined && obj.options !== undefined) {
                    let ihtml  = `<select class="backendinput" id="input_${this.keychain_id(keychain)}" tooltip="${this.get_tooltip(keychain)}">`;
                    for (let k = 0; k < obj.options.length; k++) {
                        ihtml += `<option value="${obj.options[k]}" ${(obj.options[k]==obj.val?'selected':'')}>${obj.options[k]}</option>`;
                    }
                    ihtml += `</select>`;
                    widget.innerHTML = ihtml;
                } else {
                    let keys = Object.keys(this.get_value(keychain));
                    for (let k = 0; k < keys.length; k++) {
                        this.create_widget(widget, keychain.concat([keys[k]]));
                    }
                    addlistener = false;
                }
                break;
            default:
                console.log(`BACKEND: Instance ${typeof(this.get_value(keychain))} not yet handled.`);
                return;

        }

        if (addlistener) {
            document.getElementById(`input_${this.keychain_id(keychain)}`).addEventListener('change', function (ev) {
                switch(typeof(_this.get_value(keychain))) {
                    case 'number':
                        // DEFAULT_SETTINGS[key] = Number(this.value);
                        _this.set_value(keychain, Number(this.value));
                        break;
                    case 'string':
                        // DEFAULT_SETTINGS[key] = this.value;
                        _this.set_value(keychain, this.value);
                        break;
                    case 'object':
                        var obj = _this.get_value(keychain);
                        if (obj.val !== undefined && obj.options !== undefined) {
                            obj.val = this.value;
                            _this.set_value(keychain, obj);
                            break;
                        }
                    default:
                        console.log(`BACKEND: Instance ${typeof(_this.get_value(keychain))} not yet handled.`);
                        return;
                }
                _this.value_updated(keychain);
            })
        }
    }

    toogle() {
        if (this.toogled) {
            this.raise();
            this.callback(this.updates);
        } else {
            this.lower();
            this.updates = [];
        }
        this.toogled = !this.toogled;
    }

    raise() {
        this.div.className = this.div.className.replace('backend_down', 'backend_up');
        this.handle.className = this.handle.className.replace('backendhandle_down', 'backendhandle_up');
    }

    lower() {
        this.div.className = this.div.className.replace('backend_up', 'backend_down');
        this.handle.className = this.handle.className.replace('backendhandle_up', 'backendhandle_down');
    }

    value_updated(keychain) {
        this.updates.push(keychain);
        let _this = this;
        document.getElementById(`input_${this.keychain_id(keychain)}`).className += ' backendupdated';
        setTimeout(function () {
            document.getElementById(`input_${_this.keychain_id(keychain)}`).className = document.getElementById(`input_${_this.keychain_id(keychain)}`).className.replace(' backendupdated','');
        }, 300);
    }
}
