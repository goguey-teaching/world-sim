class Layout {
    constructor() {
        this.layouts = [];
    }

    init() {
        let navs = document.querySelectorAll(".layout-nav");
        for (let k = 0; k < navs.length; k++) {
            navs[k].remove();
        }

        this.layouts = [];
        let layouts = document.querySelectorAll(".layout");
        for (let k = 0; k < layouts.length; k++) {
            let nav = document.createElement('div');
            nav.id = `layout-nav-${k}`;
            nav.className = 'layout-nav';

            this.layouts.push({
                div: layouts[k],
                children: [],
                nav: nav
            });

            let items = layouts[k].querySelectorAll(".layout-item");
            for (let l = 0; l < items.length; l++) {
                items[l].setAttribute('layout-id', `l-${l}-item`);
                if (!items[l].getAttribute('layout-name')) {
                    items[l].setAttribute('layout-name', `layout-name undefined`);
                }
                this.layouts[k].children.push({
                    div: items[l],
                    id: items[l].getAttribute('layout-id'),
                    name: items[l].getAttribute('layout-name')
                });
                if (l==0) { items[l].classList.add('active'); }

                let navitem = document.createElement('div');
                navitem.id = `layout-nav-${k}-item-${l}`;
                navitem.className = 'layout-nav-item' + (l==0?' active':'');
                navitem.innerHTML = items[l].getAttribute('layout-name');
                navitem.addEventListener('click', () => {
                    let items = layouts[k].querySelectorAll(".layout-nav-item");
                    for (let i = 0; i < items.length; i++) {
                        items[i].classList.remove('active');
                    }
                    items = layouts[k].querySelectorAll(".layout-item");
                    for (let i = 0; i < items.length; i++) {
                        items[i].classList.remove('active');
                    }
                    navitem.classList.add('active');
                    items[l].classList.add('active');
                });
                nav.appendChild(navitem)
            }
            layouts[k].prepend(nav);
        }
        
    }
}
