var SETTINGS = {}
var TOOLTIPS = {}
var backend = null;
var layout = null;
let vselections = {};

let send = null;
function connect() {
    let websocket = new WebSocket(`ws://${location.hostname}:8081/`);

    websocket.addEventListener("close", () => {
        console.log("websocket disconnected");
        send = null;
        connect();
    });
    websocket.addEventListener("error", () => {
        console.log("could not connect to websocket");
    });
    websocket.addEventListener("message", (event) => {
        let data = JSON.parse(event.data)
        if (data.type == "variables") {
            let v = [];
            
            let ini = data.data.ini;
            for (let k = 0; k < ini.length; k++) {
                SETTINGS[ini[k].name] = eval(ini[k].value);
                TOOLTIPS[ini[k].name] = ini[k].definition;
            }
            let vars = data.data.var;
            for (let k = 0; k < vars.length; k++) {
                if (vars[k].name == 'all') { continue; }
                v.push(`${vars[k].name} (${vars[k].definition})`);
            }
            multiInit("comp-v", v, ['al (Arable Land)', 'dcph (development cost per hectare)']);
            multiInit("ig-v", v, ['pop (population)']);
            backend.init();
        } else if (data.type == "img") {
            document.getElementById('world').src = data.data;
        } else if (data.type == "imgcmp") {
            document.getElementById('comp').src = data.data;
        } else if (data.type == "igraph") {
            document.getElementById('ig').data = data.data;
        }
    })
    websocket.addEventListener("open", () => {
        send = (...args) => websocket.send(...args);
    })
}
connect();

function update(updates) {
    console.log("updates");
    for (let k = 0; k < updates.length; k++) {
        send(JSON.stringify({type: 'update', name: updates[k][0], value: SETTINGS[updates[k][0]]}));
    }
    send(JSON.stringify({type: 'run'}));
}
backend = new BackEnd(SETTINGS, update, TOOLTIPS);
backend.init();
layout = new Layout();
layout.init();

document.getElementById('compare').addEventListener('click', () => {
    let v = vselections['comp-v'].getSelected().map(x => x.slice(0,x.indexOf(' (')) );
    if (v.length > 1) {
        send(JSON.stringify({type: 'compare', v: v}));
    }
})

document.getElementById('showgraph').addEventListener('click', () => {
    let d = document.getElementById('ig-d').value;
    let v = vselections['ig-v'].getSelected().map(x => x.slice(0,x.indexOf(' (')) );
    if (v.length > 0) {
        send(JSON.stringify({type: 'igraph', v: v, depth: parseInt(d)}));
    }
})

function multiInit(divid, listitems, startitems) {

    let input = document.getElementById(divid);
    if (input.classList.contains('sellect-element')) {
        let parent = input.parentElement;
        parent.replaceWith(input);
        input.classList.remove('sellect-element')
    }

    let comp = sellect(`#${divid}`, {
        originList: listitems,
        destinationList: startitems,
        // onInsert: updateCompVList,
        // onRemove: updateCompVList
    });

    // function updateCompVList(event, item) {
    //     console.log(comp.getSelected());
    // }

    comp.init();
    vselections[divid] = comp;
}
multiInit("comp-v", ['al (Arable Land)', 'dcph (development cost per hectare)'], ['al (Arable Land)', 'dcph (development cost per hectare)']);
multiInit("ig-v", ['pop (population)'], ['pop (population)']);