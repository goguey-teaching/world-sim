import os, json, inspect, numpy
import http.server, socketserver
import asyncio, websockets, threading
from pydynamo import World3
import matplotlib.pyplot as plt

HOST = ""
PORT = 8080
WSHOST = ""
WSPORT = 8081

class World:
    def __init__(self):
        self.initialvariables = []
        self.variables = []
        self.w = World3(1)
        for name, val in inspect.getmembers(self.w):
            if not name.startswith('_'):    
                if isinstance(val, (float, int, numpy.ndarray)):
                    if isinstance(val, (float)):
                        val = float(val)
                    if isinstance(val, (int)):
                        val = int(val)
                    if isinstance(val, (numpy.ndarray)):
                        val = list(val)
                    self.initialvariables.append({
                        'name': name,
                        'type': str(type(val)).replace("<class '","").replace("'>",""),
                        'value': str(val),
                        'definition': self.w.definition(name)
                    })
        vars = self.w.get_var_names()
        vars.sort()
        for v in vars:
            self.variables.append({
                'name': v,
                'definition': self.w.definition(v)
            })

    def getInitialVariables(self):
        return self.initialvariables

    def getVariables(self):
        return self.variables
    
    def updateVariable(self, name, value):
        print(name, value)
        setattr(self.w, name, value) 
    
    def run(self, name):
        plt.figure()
        self.w.run()
        self.w.plot_world(title="World scenario")
        plt.savefig('wg-{}.svg'.format(name), bbox_inches='tight')

    def compare(self, name, v):
        plt.figure()
        self.w.run()
        self.w.plot(v, rescale=True, title='Comparison between {}'.format(str(v).replace("'","")))
        plt.savefig('cmp-{}.svg'.format(name), bbox_inches='tight')

    def graph(self, name, v, depth):
        w = World3(1)
        w.run()
        w.show_influence_graph(variables=v, depth = depth).show('ig-{}.html'.format(name))

clients = {}
async def handler(websocket):
    print("New client: {}".format(websocket.id))
    clients[websocket.id] = { 'ws': websocket, 'w': World() }
    await websocket.send(json.dumps({'type': 'variables', 'data': {
            'ini': clients[websocket.id]['w'].getInitialVariables(),
            'var': clients[websocket.id]['w'].getVariables()
        }
    }))
    while True:
        try:
            message = await websocket.recv()
            data = json.loads(message)
            if data['type'] == 'run':
                clients[websocket.id]['w'].run(websocket.id)
                await websocket.send(json.dumps({'type': 'img', 'data': "wg-{}.svg".format(websocket.id)}))
            elif data['type'] == 'compare':
                clients[websocket.id]['w'].compare(websocket.id, data['v'])
                await websocket.send(json.dumps({'type': 'imgcmp', 'data': "cmp-{}.svg".format(websocket.id)}))
            elif data['type'] == 'igraph':
                clients[websocket.id]['w'].graph(websocket.id, data['v'], data['depth'])
                await websocket.send(json.dumps({'type': 'igraph', 'data': "ig-{}.html".format(websocket.id)}))
            elif data['type'] == 'update':
                clients[websocket.id]['w'].updateVariable(data['name'], data['value'])


        except websockets.ConnectionClosedOK:
            print("client connection closed.")
            del clients[websocket.id]
            os.system('rm -rf wg-{}.svg'.format(websocket.id))
            os.system('rm -rf cmp-{}.svg'.format(websocket.id))
            os.system('rm -rf ig-{}.html'.format(websocket.id))
            break
        print(" >{}".format(message))



async def wsmain():
    global WSHOST, WSPORT
    async with websockets.serve(handler, WSHOST, WSPORT):
        print("WS server started on {}:{}".format(WSHOST, WSPORT))
        await asyncio.Future()  # run forever

def thread_server():
    Handler = http.server.SimpleHTTPRequestHandler
    with socketserver.TCPServer((HOST, PORT), Handler) as httpd:
        print("serving at port", PORT)
        httpd.serve_forever()

def thread_wsserver():
    asyncio.run(wsmain())

if __name__ == "__main__":
    os.system('rm -rf wg-*.svg')
    os.system('rm -rf cmp-*.svg')
    os.system('rm -rf ig-*.html')

    server = threading.Thread(target=thread_server)
    server.start()

    thread_wsserver()
    server.join()
